#

name="${1:-id}"
if [ -e ~/.ssh/${name}_rsa ]; then
 fprint=$(ssh-keygen -l -f ~/.ssh/${name}_rsa -E sha256)
 echo "key $name already exists : $fprint"
else 
ssh-keygen -t rsa -C ${name}_rsa -f ${name}_rsa
ssh-keygen -f ${name}_rsa -y | tee ${name}_rsa.pub
ssh-keygen -l -f ~/.ssh/${name}_rsa -E sha256
ssh-keygen -l -f ~/.ssh/${name}_rsa -E MD5
fi



